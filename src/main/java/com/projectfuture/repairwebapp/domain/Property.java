package com.projectfuture.repairwebapp.domain;

import com.projectfuture.repairwebapp.enums.PropertyType;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PROPERTY")
public class Property {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "property_id")
    private Long id;

    @Column(name = "property_state_id", nullable = false, unique = true)
    private String propertyStateId;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "manufacturing_year")
    private String manufacturingYear;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", columnDefinition = "VARCHAR(45) default 'DEFAULT'")
    private PropertyType propertyType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "owner_id", nullable = false)
    private PropertyOwner propertyOwner;

    @OneToMany(mappedBy = "property", targetEntity = Repair.class, cascade = CascadeType.REMOVE)
    private List<Repair> repairs;

    public Property() {
    }

    public Property(String propertyStateId, String address, String manufacturingYear, PropertyType propertyType, PropertyOwner propertyOwner) {
        this.propertyStateId = propertyStateId;
        this.address = address;
        this.manufacturingYear = manufacturingYear;
        this.propertyType = propertyType;
        this.propertyOwner = propertyOwner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPropertyStateId() {
        return propertyStateId;
    }

    public void setPropertyStateId(String propertyStateId) {
        this.propertyStateId = propertyStateId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(String manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Property{");
        sb.append("id=").append(id);
        sb.append(", state id=").append(propertyStateId);
        sb.append(", address=").append(address);
        sb.append(", manufacturing year=").append(manufacturingYear);
        sb.append(", type=").append(propertyType.getPropertyType());
        sb.append(", property owner SSN=").append(propertyOwner.getSsn());
        sb.append('}');

        return sb.toString();
    }
}
