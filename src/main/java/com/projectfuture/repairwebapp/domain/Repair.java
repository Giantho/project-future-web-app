package com.projectfuture.repairwebapp.domain;

import com.projectfuture.repairwebapp.enums.RepairStatus;
import com.projectfuture.repairwebapp.enums.RepairType;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="REPAIR")
public class Repair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="repair_id")
    private long id;

    @Column(name="date", nullable = false)
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name="repair_status", columnDefinition = "VARCHAR(45) default 'DEFAULT'")
    private RepairStatus repairStatus;

    @Enumerated(EnumType.STRING)
    @Column(name="repair_type", columnDefinition = "VARCHAR(45) default 'DEFAULT'")
    private RepairType repairType;

    @Column(name="cost", columnDefinition = "FLOAT default '0.00'", precision=8, scale=2)
    private float cost;

    @ManyToOne(optional = false)
    @JoinColumn(name = "property_id", nullable = false)
    private Property property;

    @Column(name="description", columnDefinition = "text") //Freetext stin SQL
    private String description;

    public Repair(){

    }

    public Repair(LocalDate date, RepairStatus repairStatus, RepairType repairType, float cost, Property property, String description) {
        this.date = date;
        this.repairStatus = repairStatus;
        this.repairType = repairType;
        this.cost = cost;
        this.property = property;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public RepairStatus getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatus repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairType getRepairType() {
        return repairType;
    }

    public void setRepairType(RepairType repairType) {
        this.repairType = repairType;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Repair{");
        sb.append("id=").append(id);
        sb.append(", date=").append(date);
        sb.append(", repair status=").append(repairStatus.getRepairStatus());
        sb.append(", repair type=").append(repairType.getRepairType());
        sb.append(", cost=").append(cost);
        sb.append(", property state id=").append(property.getPropertyStateId());
        sb.append(", description=").append(description);
        sb.append('}');

        return sb.toString();


    }
}
