package com.projectfuture.repairwebapp.enums;

public enum RepairType {
    DEFAULT("Undefined"),
    PAINT_JOB("Paint job"),
    ELECTRICAL_WORK("Electrical work"),
    PLUMBING("Plumbing"),
    FRAMING("Framing");

    private String repairType;

    RepairType(String repairType) {
        this.repairType = repairType;
    }

    public String getRepairType() {
        return repairType;
    }

    public static String[] names() {
        RepairType[] types = values();
        String[] names = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            names[i] = types[i].name();
        }
        return names;
    }
}
