package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.forms.PropertyForm;
import com.projectfuture.repairwebapp.model.PropertyModel;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.PropertyService;
import com.projectfuture.repairwebapp.validators.custom.FormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class PropertyController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private FormValidator validator;

    @GetMapping("/properties")
    public String getPropertiesPage(Model model){
        List<Property> properties = propertyService.getAllProperties();
        model.addAttribute("properties", properties);
        return "pages/admin/property/list";
    }

    @GetMapping("/create-properties")
    public String getPropertiesSearchPage(){
        return "pages/admin/property/create";
    }

    @PostMapping("/create-properties")
    public String createProperty(PropertyModel propertyModel){
        if(validator.setRes("create-properties").setValues(new PropertyForm(propertyModel).getValues()).validate()){
            String ownerSsn = propertyModel.getOwnerSsn();
            propertyModel.setPropertyOwner(propertyOwnerService.findPropertyOwnerBySsn(ownerSsn));
            propertyService.createProperty(propertyModel);
            return "redirect:/admin/properties";
        }else{
            return "redirect:/admin/home";
        }
    }

    @GetMapping("/edit-property/{id}")
    public String getEditPropertyPage(@PathVariable Long id, Model model, HttpServletRequest req){
        try{
            Property property = propertyService.findProperty(id);
            model.addAttribute("property", property);
            return "pages/admin/property/edit";
        }catch (Exception e){
            return "redirect:/admin/home";
        }

    }

    @PostMapping("/edit-property/{id}")
    public String editProperty(@PathVariable Long id, PropertyModel propertyModel, HttpServletRequest req){
        String currentStateId = propertyService.findProperty(id).getPropertyStateId();
        if(validator.setRes("edit-property").setValues(new PropertyForm(propertyModel).getValues()).setSpecial(currentStateId).validate()){
            String ownerSsn = propertyModel.getOwnerSsn();
            propertyModel.setPropertyOwner(propertyOwnerService.findPropertyOwnerBySsn(ownerSsn));
            propertyService.updateProperty(propertyModel);
            return "redirect:/admin/properties";
        }else{
            return "redirect:/admin/home";
        }
    }

    @PostMapping("/remove-property/{id}")
    public String removeProperty(@PathVariable Long id){
        propertyService.deleteProperty(id);
        return "redirect:/admin/properties";
    }

    @GetMapping("/search-properties")
    public String getSearchPropertyPage(){
        return "pages/admin/property/search";
    }

    @PostMapping("/search-properties")
    public String searchPropertyResult(@RequestParam(name = "propertyStateId") String propertyStateId,
                                       @RequestParam(name = "ownerSsn") String ownerSsn,
                                       Model model){
        try {
            List<Property> properties = new ArrayList<>();
            if (!propertyStateId.equals("")) {
                properties.add(propertyService.findPropertyByPropertyStateId(propertyStateId));
            } else if (!ownerSsn.equals("")) {
                properties = propertyService.findPropertyByPropertyOwnerSsn(ownerSsn);
            }
            if (properties != null) {
                model.addAttribute("properties", properties);
            }

            model.addAttribute("propertyStateId", propertyStateId);
            model.addAttribute("ownerSsn", ownerSsn);
        }catch (Exception e){
            model.addAttribute("no_results","No results found...");
        }
        return "pages/admin/property/search";
    }
}
