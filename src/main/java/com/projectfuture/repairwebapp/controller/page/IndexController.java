package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    RepairService repairService;

    @GetMapping("/")
    public String getRootPage(Model model){

        Authentication authContext = SecurityContextHolder.getContext().getAuthentication();

        if (!(authContext instanceof AnonymousAuthenticationToken)) {
            if (authContext.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ADMIN"))) {
                return "redirect:/admin/home";
            }
            return "redirect:/user/home";
        }

        return "redirect:/login";

    }

    @GetMapping("admin/home")
    public String getAdminHomePage(Model model){
        List<Repair> repairs = repairService.findRepairByDateIsAfterOrderByDateAsc(LocalDate.now());
        model.addAttribute("repairs", repairs);
        return "pages/admin/home";
    }

    @GetMapping("user/home")
    public String getUserHomePage(Model model, HttpServletRequest request){
        String email = request.getSession().getAttribute("username").toString();
        List<Repair> repairs = repairService.findRepairByPropertyOwnerEmail(email);
        model.addAttribute("repairs", repairs);
        return "pages/user/home";
    }

}
