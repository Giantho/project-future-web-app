package com.projectfuture.repairwebapp.forms;

import com.projectfuture.repairwebapp.model.PropertyOwnerModel;

public class PropertyOwnerForm {

    private String[] values;

    public PropertyOwnerForm(PropertyOwnerModel propertyOwnerModel) {
        String ssn = propertyOwnerModel.getSsn().trim();
        String firstName = propertyOwnerModel.getFirstName().trim();
        String lastName = propertyOwnerModel.getLastName().trim();
        String phone = propertyOwnerModel.getPhoneNumber().trim();
        String email = propertyOwnerModel.getEmail().trim();
        this.values = new String[]{ssn,firstName,lastName,phone,email};
    }

    public String[] getValues(){
        return this.values;
    }

}
