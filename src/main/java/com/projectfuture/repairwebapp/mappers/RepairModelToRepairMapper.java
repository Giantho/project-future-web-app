package com.projectfuture.repairwebapp.mappers;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.model.RepairModel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RepairModelToRepairMapper {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static Repair mapTo(RepairModel repairModel){
        Repair repair = new Repair();
        repair.setDate(LocalDate.parse(repairModel.getDate(), DATE_TIME_FORMATTER));
        repair.setRepairStatus(repairModel.getRepairStatus());
        repair.setRepairType(repairModel.getRepairType());
        repair.setCost(repairModel.getCost());
        repair.setProperty(repairModel.getProperty());
        repair.setDescription(repairModel.getDescription());

        return repair;
    }
}
