package com.projectfuture.repairwebapp.mappers;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.model.PropertyModel;

public class PropertyModelToProperty {

    public static Property mapTo(PropertyModel propertyModel){
        Property property = new Property();
        property.setPropertyStateId(propertyModel.getPropertyStateId());
        property.setAddress(propertyModel.getAddress());
        property.setManufacturingYear(propertyModel.getManufacturingYear());
        property.setPropertyType(propertyModel.getPropertyType());
        property.setPropertyOwner(propertyModel.getPropertyOwner());

        return property;
    }
}
