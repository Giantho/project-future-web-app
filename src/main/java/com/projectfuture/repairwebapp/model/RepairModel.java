package com.projectfuture.repairwebapp.model;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.enums.RepairStatus;
import com.projectfuture.repairwebapp.enums.RepairType;

public class RepairModel {

    private long id;
    private String date;
    private RepairStatus repairStatus;
    private RepairType repairType;
    private float cost;
    private String propertyStateId;
    private Property property;
    private String description;

    public RepairModel(){ }

    public RepairModel(long id, String date, RepairStatus repairStatus, RepairType repairType, float cost, String propertyStateId, Property property, String description) {
        this.id = id;
        this.date = date;
        this.repairStatus = repairStatus;
        this.repairType = repairType;
        this.cost = cost;
        this.propertyStateId = propertyStateId;
        this.property = property;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RepairStatus getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatus repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairType getRepairType() {
        return repairType;
    }

    public void setRepairType(RepairType repairType) {
        this.repairType = repairType;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getPropertyStateId() {
        return propertyStateId;
    }

    public void setPropertyStateId(String propertyStateId) {
        this.propertyStateId = propertyStateId;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
