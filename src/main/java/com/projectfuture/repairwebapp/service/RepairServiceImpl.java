package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.mappers.RepairModelToRepairMapper;
import com.projectfuture.repairwebapp.model.RepairModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import com.projectfuture.repairwebapp.repository.PropertyRepository;
import com.projectfuture.repairwebapp.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RepairServiceImpl implements RepairService {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Autowired
    private PropertyRepository propertyRepository;

    @Override
    public Repair findRepair(Long id) {
        return repairRepository.findById(id).get();
    }

    @Override
    public List<Repair> getAllRepairs() {
        return repairRepository.findAll();
    }

    @Override
    public List<Repair> findRepairByDate(LocalDate date) {

        return repairRepository.findRepairByDate(date);
    }

    @Override
    public List<Repair> findRepairByDateBetweenOrderByDate(LocalDate from, LocalDate until) {
        return repairRepository.findRepairByDateBetweenOrderByDate(from, until);
    }

    @Override
    public List<Repair> findRepairByPropertyOwnerSsn(String ssn) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findPropertyOwnerBySsn(ssn).get();
        List<Property> properties = propertyRepository.findPropertyByPropertyOwner(propertyOwner);
        List<Repair> repairs = new ArrayList<>();

        for (Property property: properties) {
            repairs.addAll(repairRepository.findRepairByProperty(property));
        }

        return repairs;
    }

    @Override
    public List<Repair> findRepairByPropertyOwnerEmail(String email) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findPropertyOwnerByEmail(email).get();
        List<Property> properties = propertyRepository.findPropertyByPropertyOwner(propertyOwner);
        List<Repair> repairs = new ArrayList<>();

        for (Property property: properties) {
            repairs.addAll(repairRepository.findRepairByProperty(property));
        }

        return repairs;
    }

    @Override
    public List<Repair> findRepairByDateIsAfterOrderByDateAsc(LocalDate date) {
        List<Repair> repairs = repairRepository.findRepairByDateIsAfterOrderByDateAsc(date);
        int repairsAmount = repairs.size();
        if(repairsAmount<10){
            repairs = repairs.subList(0,repairsAmount);
        } else {
            repairs = repairs.subList(0,10);
        }

        return repairs;
    }

    @Override
    public Repair createRepair(RepairModel repairModel) {
        Repair repair = RepairModelToRepairMapper.mapTo(repairModel);
        return repairRepository.save(repair);
    }

    @Override
    public Repair updateRepair(RepairModel repairModel) {
        Repair originalRepair= RepairModelToRepairMapper.mapTo(repairModel);
        originalRepair.setId(repairModel.getId());
        return repairRepository.save(originalRepair);
    }


    @Override
    public void deleteRepairById(long id) {
        repairRepository.deleteRepairById(id);
    }
}
