package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.model.LoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    PropertyOwnerService propertyOwnerService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        PropertyOwner user = propertyOwnerService.findPropertyOwnerByEmail(username);
        return new LoginResponse(user.getUsername(), user.getPassword(), Arrays.asList(new SimpleGrantedAuthority(user.getRole().name())));
    }
}
