package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.model.RepairModel;

import java.time.LocalDate;
import java.util.List;

public interface RepairService {

    Repair findRepair(Long id);

    List<Repair> getAllRepairs();

    List<Repair> findRepairByDate(LocalDate date);

    List<Repair> findRepairByDateBetweenOrderByDate(LocalDate from, LocalDate until);

    List<Repair> findRepairByPropertyOwnerSsn(String ssn);

    List<Repair> findRepairByPropertyOwnerEmail(String email);

    List<Repair> findRepairByDateIsAfterOrderByDateAsc(LocalDate date);

    Repair createRepair(RepairModel repairModel);

    Repair updateRepair(RepairModel repairModel);

    void deleteRepairById(long id);

}
